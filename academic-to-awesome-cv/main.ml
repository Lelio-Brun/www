module L = Sedlexing.Utf8
module E = Ezjsonm
module Y = Yaml
module O = Omd
module D = ISO8601.Permissive

let root_dir = Sys.getcwd () ^ "/.."
let index = root_dir ^ "/content/_index.md"

(** YAML Sedlex lexers *)

let yaml_delim = [%sedlex.regexp? "---"]

let yaml buf =
  let rec tok buf = match%sedlex buf with
    | yaml_delim -> ""
    | any -> let s = L.lexeme buf in s ^ tok buf (* WHY??? *)
    | _ -> failwith "YAML front-matter unexpectedly terminated"
  in
  tok buf
  |> Y.of_string
  |> Result.get_ok

let rec yaml_token buf =
  match%sedlex buf with
  | yaml_delim -> yaml buf
  | any -> yaml_token buf
  | _ -> failwith "No YAML front-matter found"

let process lexer file process =
  let ic = open_in file in
  let lexbuf = L.from_channel ic in
  let data = lexer lexbuf in
  process data;
  close_in ic

let process_yaml = process yaml_token

let json_to_string json =
  let s = E.value_to_string json in
  let s = if s.[0] = '"' && s.[String.length s - 1] = '"'
          then String.sub s 1 (String.length s - 2)
          else s in
  String.trim s

let find_string json key =
  E.find json [key]
  |> json_to_string

let process_index id f =
  process_yaml index (fun json ->
    let json = E.find json ["sections"]
               |> E.get_list (fun block -> find_string block "id", block)
               |> List.assoc id
    in
    E.find json ["content"] |> f)

(** Markdown to LaTeX *)

let md_to_latex =
  let open Format in
  (* let md_to_latex_aux fp md = *)
  let open O in
  let pp_cmd cmd pp fp = fprintf fp "\\%s{%a}" cmd pp in
  let pp_env env pp fp x = fprintf fp "\\begin{%s}%a\\end{%s}" env pp x env in
  let pp_item pp fp = fprintf fp "\\item %a" pp in
  let sanitize s =
    let open Re.Str in
    s
    |> global_replace (regexp "\\\\n") "\\\\newline "
    |> global_replace (regexp "--") "\\\\textendash{}"
  in
  let rec inline_to_latex fp = function
    | Concat (_, mds) ->
      pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt "") inline_to_latex fp mds
    | Text (_, s) ->
      pp_print_string fp (sanitize s)
    | Emph (_, md) ->
      pp_cmd "emph" inline_to_latex fp md
    | Strong (_, md) ->
      pp_cmd "textbf" inline_to_latex fp md
    | Code (_, c) ->
      pp_env "verbatim" pp_print_string fp c
    | Hard_break _ ->
      fprintf fp "@;@;"
    | Soft_break _ ->
      pp_print_string fp "\\\\"
    | Link (_, { label; destination; _ }) ->
      pp_cmd (sprintf "href{%s}" destination) inline_to_latex fp label
    | Html (_, "sup") ->
      pp_cmd "textsuperscript" (fun _fmt () -> ()) fp ()
    | _ -> ()
  in
  let heading_to_latex fp i md =
    fprintf fp "\\%s{%a}@;@;"
      (match i with
       | 1 -> "chapter"
       | 2 -> "section"
       | 3 -> "subsection"
       | 4 -> "subsubsection"
       | 5 -> "paragraph"
       | _ -> "subparagraph")
      inline_to_latex md
  in
  let rec block_to_latex fp = function
    | Paragraph (_, md) ->
      inline_to_latex fp md
    | List (_, Ordered _, _, mds) ->
      pp_env "enumerate" (pp_print_list (pp_item blocks_to_latex)) fp mds
    | List (_, Bullet _, _, mds) ->
      pp_env "itemize" (pp_print_list (pp_item blocks_to_latex)) fp mds
    | Blockquote (_, mds) ->
      pp_env "quotation" blocks_to_latex fp mds
    | Heading (_, i, inl) ->
      heading_to_latex fp i inl
    (* | Thematic_break of 'attr *)
    (* | Code_block of 'attr * string * string *)
    (* | Html_block of 'attr * string *)
    (* | Definition_list of 'attr * 'attr def_elt list *)
    | _ -> assert false
  and blocks_to_latex fp = pp_print_list block_to_latex fp in
  blocks_to_latex

(** Process main author file *)

type organization = {
    org_name: string;
    org_url: string
  }

type course = {
    cou_name: O.doc;
    cou_institution: O.doc;
    cou_year: string;
    cou_location: string;
    cou_extra: O.doc
  }

let pp_course fmt course =
  (* print_endline (O.to_sexp course.cou_extra); *)
  Format.fprintf fmt
    "\\cventry{%a}{%a}{%s}{%s}{%a}"
    md_to_latex course.cou_institution
    md_to_latex course.cou_name
    course.cou_location
    course.cou_year
    md_to_latex course.cou_extra

let avatar = "avatar.jpg"

let first_name = ref ""
let last_name = ref ""
let position = ref ""
let email = ref ""
let github = ref None
let gitlab = ref None
let googlescholar = ref None
let linkedin = ref None
let dblp = ref None
let orcid = ref None
let hal = ref None

let education = ref []

let get_gravatar () =
  let open Lwt in
  let open Cohttp_lwt in
  let open Cohttp_lwt_unix in
  let hash = Digest.(string !email |> to_hex) in
  let url = Uri.of_string ("https://gravatar.com/avatar/" ^ hash ^ "?s=800") in
  let s = (Client.get url >>= fun (_, body) -> Body.to_string body)
          |> Lwt_main.run
  in
  let oc = open_out avatar in
  output_string oc s;
  close_out oc

let process_author json =
  let find_string' = find_string json in
  begin match find_string' "title" |> String.split_on_char ' ' with
  | [f; l] -> first_name := f; last_name := l
  | _ -> assert false
  end;
  position := find_string' "role";
  email := find_string' "email" |> String.lowercase_ascii;
  (* let bio = find_string' "bio" in *)
  (* let organizations =
   *   List.assoc "organizations" dict
   *   |> E.get_list (fun json ->
   *       let dict = E.get_dict json in
   *       let org_name = find_string dict "name" in
   *       let org_url = find_string dict "url" in
   *       { org_name; org_url })
   * in *)
  (* let interests =
   *   List.assoc "interests" dict
   *   |> E.get_list json_to_string
   * in *)
  education :=
    E.find json ["education"; "courses"]
    |> E.get_list (fun json ->
           let cou_name = O.of_string (find_string json "course") in
           let cou_institution = O.of_string (find_string json "institution") in
           let cou_year = find_string json "year" in
           let cou_location = find_string json "location" in
           let cou_extra = O.of_string (find_string json "extra") in
           { cou_name; cou_institution; cou_year; cou_location; cou_extra })
    |> List.sort (fun ed1 ed2 -> compare ed2.cou_year ed1.cou_year);
  let get_path n uri =
    let path = Uri.path uri in
    String.sub path n (String.length path - n)
  in
  let social =
    E.find json ["social"]
    |> E.get_list (fun json ->
           find_string json "icon", find_string json "link" |> Uri.of_string)
  in
  let (>>|) o f = Option.map f o in
  let (>>=) = Option.bind in
  github := List.assoc_opt "github" social >>| get_path 1;
  gitlab := List.assoc_opt "gitlab" social >>| get_path 1;
  googlescholar := List.assoc_opt "google-scholar" social >>= (fun uri ->
    Uri.get_query_param uri "user");
  linkedin := List.assoc_opt "linkedin" social >>| get_path 4;
  dblp := List.assoc_opt "dblp" social >>| get_path 5;
  orcid := List.assoc_opt "orcid" social >>| get_path 1;
  hal := List.assoc_opt "hal" social >>| get_path 1;
  get_gravatar ()

let process_author_file () =
  process_yaml (root_dir ^ "/content/authors/admin/_index.md") process_author

(** Process params file *)

type address = {
    add_institution: string;
    add_street: string;
    add_postcode: string;
    add_city: string;
    add_country: string;
  }

let address = ref {
                  add_institution = "";
                  add_street = "";
                  add_postcode = "";
                  add_city = "";
                  add_country = ""
                }

let pp_address fmt a =
  Format.fprintf fmt "%s, %s, %s, %s, %s"
    a.add_institution
    a.add_street a.add_postcode a.add_city a.add_country

let process_contact json =
  let add = E.find json ["address"] in
  let add_street = find_string add "street" in
  let add_postcode = find_string add "postcode" in
  let add_city = find_string add "city" in
  let add_country = find_string add "country" in
  let inst = E.find json ["institution"] in
  let add_institution = find_string inst "name" in
  address := { add_institution; add_street; add_postcode; add_city; add_country }

(** Process experience *)

type experience = {
    exp_title: string;
    exp_company: O.doc;
    exp_url: string;
    exp_location: string;
    exp_start: float;
    exp_end: float option;
    exp_description: O.doc
  }

let month_to_string = function
  | 0 -> "January"
  | 1 -> "February"
  | 2 -> "March"
  | 3 -> "April"
  | 4 -> "May"
  | 5 -> "June"
  | 6 -> "July"
  | 7 -> "August"
  | 8 -> "September"
  | 9 -> "October"
  | 10 -> "November"
  | 11 -> "December"
  | _ -> assert false

let pp_date fmt d = let open Unix in
                    let d = gmtime d in
                    Format.fprintf fmt "%s. %i"
                      (String.sub (month_to_string d.tm_mon) 0 3) (d.tm_year + 1900)

let pp_dates fmt (d_start, d_end) =
  Format.(fprintf fmt "%a \\textendash{} %a"
            pp_date d_start
            (pp_print_option ~none:(fun fmt () -> pp_print_string fmt "Present")
               pp_date) d_end)
(* (fun fmt d -> D.pp_format fmt "%M %Y" d 0.) d_start
 * (fun fmt d -> match d with
 *    | None -> Format.pp_print_string fmt "Present"
 *    | Some d -> D.pp_format fmt "%M %Y" d 0.) d_end *)
(* (match d_end with None -> "Present" | Some d -> D.format d "%b. %Y") *)

let pp_experience fmt exp =
  Format.fprintf fmt
    "\\cventry{\\href{%s}{%a}}{%s}{%s}{%a}{%a}"
    exp.exp_url
    md_to_latex exp.exp_company
    exp.exp_title
    exp.exp_location
    pp_dates (exp.exp_start, exp.exp_end)
    md_to_latex exp.exp_description

let experiences = ref []

let process_experience json =
  (* print_endline (Y.to_string_exn json); *)
  let json = E.find json ["items"] in
  experiences := E.get_list (fun json ->
                    let exp_title = find_string json "title" in
                    let exp_company = find_string json "company" |> O.of_string in
                    let exp_url = find_string json "company_url" in
                    let exp_location = find_string json "location" in
                    let exp_start = find_string json "date_start" |> D.date in
                    let exp_end = find_string json "date_end" in
                    let exp_end = if exp_end = "" then None else Some (D.date exp_end) in
                    let exp_description = find_string json "description" |> O.of_string in
                    { exp_title; exp_company; exp_url; exp_location;
                      exp_start; exp_end; exp_description })
                  json
                |> List.sort (fun e1 e2 -> compare e2.exp_start e1.exp_start)

(** Process teaching *)

type teaching = {
    tea_title: string;
    tea_year: string;
    tea_url: string;
    tea_university: string;
    tea_level: O.doc;
  }

let sanitize s =
  let open Re.Str in
  s
  |> global_replace (regexp "_") "\\_"
  |> global_replace (regexp "#") "\\#"
  |> global_replace (regexp "%") "\\%"

let pp_teaching =
  let years = Hashtbl.create 16 in
  fun fmt tea ->
    Format.fprintf fmt
      "\\cvhonor{\\href{%s}{%s}}{%a}{%s}{%s}"
      (sanitize tea.tea_url)
      tea.tea_title
      md_to_latex tea.tea_level
      tea.tea_university
      (if Hashtbl.mem years tea.tea_year then ""
       else Re.Str.(global_replace (regexp "--") "\\\\textendash{}"
                      tea.tea_year));
    Hashtbl.add years tea.tea_year ()

let assistants = ref []

let process_teaching json =
  let json = E.find json ["assistant"] in
  assistants := E.get_list (fun json ->
      let tea_title = find_string json "title" in
      let tea_year = find_string json "year" in
      let tea_url = find_string json "url" in
      let tea_university = find_string json "university" in
      let tea_level = find_string json "level" |> O.of_string in
      { tea_title; tea_year; tea_url; tea_university; tea_level })
      json
                |> List.sort (fun a1 a2 -> compare a2.tea_year a1.tea_year)

(** Process academic services *)

type service = {
    ser_year: string;
    ser_event: string;
    ser_url: string;
    ser_role: string;
  }

let pp_service =
  let years = Hashtbl.create 16 in
  fun fmt ser ->
    Format.fprintf fmt
      "\\cvhonor{%s}{}{\\href{%s}{%s}}{%s}"
      (String.capitalize_ascii ser.ser_role)
      ser.ser_url ser.ser_event
      (if Hashtbl.mem years ser.ser_year then "" else ser.ser_year);
    Hashtbl.add years ser.ser_year ()

let services = ref []

let process_services json =
  let json = E.find json ["items"] in
  (* let services_t = get t (key "service" |-- array |-- tables) |> Option.get in *)
  services := E.get_list (fun json ->
                 let ser_year = find_string json "year" in
                 let ser_event = find_string json "event" in
                 let ser_url = find_string json "url" in
                 let ser_role = find_string json "role" in
                 { ser_year; ser_event; ser_url; ser_role })
      json
             |> List.sort (fun ser1 ser2 -> compare ser2.ser_year ser1.ser_year)

(** Process publis *)

let bibs = ref []

let pp_bib fmt =
  Format.fprintf fmt "\\addbibresource{%s}"

let process_publications () =
  let pub_dir = root_dir ^ "/content/publication" in
  bibs := Sys.readdir pub_dir
         |> Array.to_list
         |> List.filter_map (fun f ->
                let d = Format.sprintf "%s/%s" pub_dir f in
                if Sys.is_directory d
                then Some (Format.sprintf "%s/cite.bib" d)
                else None)

(** Process config file *)

let homepage = ref ""

let process_config y =
  let open Y.Util in
  homepage := find_exn "baseurl" y |> Option.get |> to_string_exn
             |> Uri.of_string |> Uri.host |> Option.get

let read_whole_file f =
  let ch = open_in f in
  let s = really_input_string ch (in_channel_length ch) in
  close_in ch;
  s

let process_config_file () =
  let s = read_whole_file (root_dir ^ "/config/_default/hugo.yaml") in
  let y = s |> Y.of_string |> Result.get_ok in
  process_config y

(** Print & compile LaTeX *)

let cv_file = "CV.tex"

let print () =
  let open Format in
  let oc = open_out cv_file in
  let fp = formatter_of_out_channel oc in
  let pp_social name fmt =
    pp_print_option (fun fmt id ->
        fprintf fmt "\\%s{%s}@;" name id) fmt
  in
  let pp_break fmt () = fprintf fmt "@;" in
  let pp_break2 fmt () = fprintf fmt "@;@;" in
  fprintf fp
    "@[<v>\\documentclass[11pt, a4paper]{awesome-cv}@;@;\
     \\usepackage[sorting=ydnt,bibstyle=awesome-cv,maxbibnames=10]{biblatex}@;@;\
     \\geometry{left=1.4cm, top=1cm, right=1.4cm, bottom=1cm, footskip=0cm}@;\
     \\definecolor{awesome}{HTML}{1565c0}@;\
     \\setbool{acvSectionColorHighlight}{true}@;\
     \\renewcommand{\\acvHeaderSocialSep}{\\quad\\textbar\\quad}@;@;\
     \\photo{%s}@;\
     \\name{%s}{%s}@;\
     \\position{%s}@;\
     \\address{%a}@;\
     \\mobile{(+81) (0) 80 8358 4207}@;\
     \\email{%s}@;\
     \\homepage{%s}@;\
     %a%a%a%a%a%a%a@;\
     %a@;\
     \\nocite{*}@;@;\
     \\begin{document}@;@;\
     \\makecvheader@;\
     \\makecvfooter{}{\\thepage}{}@;@;\
     \\cvsection{Education}@;@;\
     @[<v 2>\\begin{cventries}@;%a@]@;\\end{cventries}@;@;\
     \\cvsection{Experience}@;@;\
     @[<v 2>\\begin{cventries}@;%a@]@;\\end{cventries}@;@;\
     \\cvsection{Skills}@;@;\
     \\begin{cvskills}@;\
     \\cvskill{Programming}{OCaml, Haskell, Rust, C, C++, Java, Python, HTML / CSS, Javascript, SQL, \\LaTeX, Git}@;\
     \\cvskill{Formal Methods}{Coq Proof Assistant, Frama-C Software Analyzers, Tamarin Prover}@;\
     \\cvskill{Languages}{French (native), English (fluent), Japanese (beginner), Spanish (beginner)}@;\
     \\end{cvskills}@;@;\
     \\pagebreak@;\
     \\cvsection{Publications}@;@;\
     \\printbibliography[heading=none, env=cvbib]@;@;\
     \\cvsection{Teaching assistant}@;@;\
     @[<v 2>\\begin{cvhonors}[2.3cm]@;%a@]@;\\end{cvhonors}@;@;\
     \\cvsection{Academic service}@;@;\
     @[<v 2>\\begin{cvhonors}@;%a@]@;\\end{cvhonors}@;@;\
     \\end{document}\
     @]@."
    avatar !first_name !last_name !position
    pp_address !address
    !email !homepage
    (pp_social "github") !github
    (pp_social "gitlab") !gitlab
    (pp_social "googlescholar") !googlescholar
    (pp_social "linkedin") !linkedin
    (pp_social "orcid") !orcid
    (pp_social "dblp") !dblp
    (pp_social "hal") !hal
    (pp_print_list ~pp_sep:pp_break pp_bib)
    !bibs
    (pp_print_list ~pp_sep:pp_break2 pp_course)
    !education
    (pp_print_list ~pp_sep:pp_break2 pp_experience)
    !experiences
    (pp_print_list ~pp_sep:pp_break2 pp_teaching)
    !assistants
    (pp_print_list ~pp_sep:pp_break2 pp_service)
    !services
  ;
  close_out oc

let compile () =
  Sys.command ("latexmk -xelatex " ^ cv_file) |> ignore

let () =
  try
  process_author_file ();
  process_index "contact" process_contact;
  process_config_file ();
  process_index "experience" process_experience;
  process_index "teaching" process_teaching;
  process_index "academic" process_services;
  process_publications ();
  print ();
  compile ()
  with Not_found -> failwith "ah"
