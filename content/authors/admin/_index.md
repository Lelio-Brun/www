---
# Display name
title: Lelio Brun

# Username (this should match the folder name)
authors:
  - admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Postdoctoral research scientist in Computer Science

# Organizations/Affiliations
organizations:
  - name: National Institute of Informatics
    url: "https://www.nii.ac.jp/en/"

# Short bio (displayed in user profile at end of posts)
bio:
  My research interests include synchronous languages, functional programming and verified compilation.

interests:
  - Functional programming
  - Synchronous languages
  - Proof assistants
  - Security protocols analysis

education:
  courses:
    - course: PhD in Computer Science
      institution: École normale supérieure - PSL Research University
      year: 2020
      location: Paris, France
      extra: "*Mechanized Semantics and Verified Compilation for a Dataflow Synchronous Language with Reset*

      Honors: GDR GPL *accessit* prize"
    - course: MSc in Computer Science ([MPRI](https://wikimpri.dptinfo.ens-cachan.fr/))
      institution: Université Paris Cité
      year: 2016
      location: Paris, France
      extra: "Honors: *summa cum laude*"
    - course: MSc in Aerospace Engineering
      institution: ISAE-ENSMA
      year: 2013
      location: Poitiers, France
      extra: exchange at ÉTS, Montréal, Canada

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
#- icon: envelope
#  icon_pack: fas
#  link: '#contact'  # For a direct email link, use "mailto:test@example.org".
#- icon: twitter
#  icon_pack: fab
#  link: https://twitter.com/GeorgeCushen
- icon: dblp 
  icon_pack: ai 
  link: https://dblp.org/pid/201/4806
- icon: hal 
  icon_pack: ai 
  link: https://cv.archives-ouvertes.fr/lelio-brun
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=DeY3Y3IAAAAJ
- icon: orcid 
  icon_pack: ai
  link:  https://orcid.org/0000-0002-0642-6008  
# - icon: linkedin
  # icon_pack: fab
  # link: https://www.linkedin.com/in/leliobrun
- icon: github
  icon_pack: fab
  link: https://github.com/Lelio-Brun
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/Lelio-Brun
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "lb@leliobrun.net"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups: []
---
# Biography

I am a postdoctoral research scientist in the {{< url ZT-IoT true >}} project at
{{< url NII >}} in Tokyo.

I defended my thesis in 2020, {{< staticref "files/thesis.pdf" >}}Mechanized
Semantics and Verified Compilation for a Dataflow Synchronous Language with
Reset{{< /staticref >}}, written under the supervision of {{< url mpouzet >}}
and {{< url tbourke >}}. 
The thesis was awarded an [_accessit_ prize](https://gdr-gpl.cnrs.fr/node/444)
from the [GDR GPL](https://gdr-gpl.cnrs.fr/) French research group.

I currently work on applying formal methods to ensure security of Internet of
Things (IoT) systems.
