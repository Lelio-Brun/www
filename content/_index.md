---
# Leave the homepage title empty to use the site title
title:
type: landing

sections:
  - block: v1/about
    id: about
    Params:
      author: admin
      title: Biography

  - block: collection
    id: publications
    content:
      title: Publications
      # Choose how many pages you would like to display (0 = all pages)
      count: 0
      # Filter on criteria
      filters:
        # The folders to display content from
        folders:
          - publication
        author: ""
        category: ""
        tag: ""
        publication_type: ""
        featured_only: false
        exclude_featured: false
        exclude_future: false
        exclude_past: false
      # Choose how many pages you would like to offset by
      # Useful if you wish to show the first item in the Featured widget
      offset: 0
      # Field to sort by, such as Date or Title
      sort_by: 'Date'
      sort_ascending: false
    design:
      # Choose a listing view
      view: community/custom
      # Choose single or dual column layout
      columns: '2'

  - block: collection
    id: talks
    content:
      title: Talks
      # Choose how many pages you would like to display (0 = all pages)
      count: 5
      # Filter on criteria
      filters:
        # The folders to display content from
        folders:
          - talk
        author: ""
        category: ""
        tag: ""
        publication_type: ""
        featured_only: false
        exclude_featured: false
        exclude_future: false
        exclude_past: false
      # Choose how many pages you would like to offset by
      # Useful if you wish to show the first item in the Featured widget
      offset: 0
      # Field to sort by, such as Date or Title
      sort_by: 'Date'
      sort_ascending: false
    design:
      # Choose a listing view
      view: community/custom
      # Choose single or dual column layout
      columns: '2'

  - block: portfolio
    id: projects
    content:
      title: Projects
      filters:
        # Folders to display content from
        folders:
          - project
        # Only show content with these tags
        tags: []
        # Exclude content with these tags
        exclude_tags: []
        # Which Hugo page kinds to show (https://gohugo.io/templates/section-templates/#page-kinds)
        kinds:
          - page
      # Field to sort by, such as Date or Title
      sort_by: 'Date'
      sort_ascending: false
      # Default portfolio filter button
      # 0 corresponds to the first button below and so on
      # For example, 0 will default to showing all content as the first button below shows content with *any* tag
      default_button_index: 0
      # Filter button toolbar (optional).
      # Add or remove as many buttons as you like.
      # To show all content, set `tag` to "*".
      # To filter by a specific tag, set `tag` to an existing tag name.
      # To remove the button toolbar, delete the entire `buttons` block.
      # buttons:
      #   - name: All
      #     tag: '*'
      #   - name: Deep Learning
      #     tag: Deep Learning
      #   - name: Other
      #     tag: Demo
    design:
      # See Page Builder docs for all section customization options.
      # Choose how many columns the section has. Valid values: '1' or '2'.
      columns: '2'
      # Choose a listing view
      view: masonry
      # For Showcase view, flip alternate rows?
      flip_alt_rows: false

  - block: experience
    id: experience
    content:
      title: Experience
      # Date format for experience
      #   Refer to https://wowchemy.com/docs/customization/#date-format
      date_format: Jan 2006
      # Experiences.
      #   Add/remove as many experience `items` below as you like.
      #   Required fields are `title`, `company`, and `date_start`.
      #   Leave `date_end` empty if it's your current employer.
      #   Begin multi-line descriptions with YAML's `|2-` multi-line prefix.
      items:
        - title: Postdoctoral research scientist
          company: NII -- ZT-IoT project
          company_url: https://www.nii.ac.jp/research/projects/jst-crest/
          company_logo: nii
          location: Tokyo, Japan
          date_start: 2022-10-17
          date_end: ''
          description: 'Formal security analysis for IoT systems using the Tamarin Prover.'
        - title: Postdoctoral research scientist
          company: ISAE-SUPAERO -- IpSC Team (DISC)
          company_url: https://www.isae-supaero.fr/fr/recherche/departements/ingenierie-systemes-complexes/groupe-ingenierie-pour-les-systemes-critiques-460/
          company_logo: supaero
          location: Toulouse, France
          date_start: 2021-01-18
          date_end: '2022-08-31'
          description: 'Adding translation validation features to the LustreC Lustre to C compiler through the Frama-C platform.'
        - title: Postdoctoral research engineer
          company: École normale supérieure / Inria -- PARKAS Team
          company_url: https://parkas.di.ens.fr/
          company_logo: inria
          location: Paris, France
          date_start: 2020-01-01
          date_end: 2020-12-31
          description: 'Adding enumerations and case analysis support to the Vélus Lustre to Assembly compiler verified with the Coq Proof Assistant.'
        - title: Software engineer
          company: Astek
          company_url: https://astekgroup.fr/
          company_logo: astek
          location: Le Plessis Robinson, France
          date_start: 2014-05-01
          date_end: 2014-08-31
          description: 'Bug tracking and corrections in a large C++ codebase.'
    design:
      # Choose how many columns the section has. Valid values: '1' or '2'.
      columns: '2'

  - block: teaching
    id: teaching
    content:
      title: Teaching
      assistant:
        - title: 'Computer science'
          year: '2021/22'
          url: 'https://www.isae-supaero.fr/IMG/pdf/catalogue_2021-2022_1a.pdf#CAt-A1-2021-2022.indd%3A.35055%3A1270'
          university: 'ISAE-SUPAERO'
          level: '1<sup>st</sup> year graduate'
        - title: 'Languages translation'
          year: '2021/22'
          url:  'http://formations.enseeiht.fr/en/our-courses/diplome-d-ingenieur-FC_DI/diplome-D/ingenieur-enseeiht-informatique-et-telecommunications-program-program1-n7i5-171-en/ingenieur-enseeiht-informatique-et-telecommunications-2eme-annee-subprogram-subprogram-n7i52-181-en/annee-2a-sn-fise-NDEN/choix-de-parcours-semestre-7-2a-sn-fise-N7ENA/semestre-7-sn-fise-parcours-image-et-multimedia-N7ENAM/base-de-la-programmation-fonct-et-traduction-des-langages-N7EN08/traduction-des-langages-N7EN08B.html'
          university: 'ENSEEIHT'
          level: '1<sup>st</sup> year graduate'
        - title: 'Functional programming'
          year: '2021/22'
          url:  'http://formations.enseeiht.fr/en/our-courses/diplome-d-ingenieur-FC_DI/diplome-D/ingenieur-enseeiht-informatique-et-telecommunications-program-program1-n7i5-171-en/ingenieur-enseeiht-informatique-et-telecommunications-2eme-annee-subprogram-subprogram-n7i52-181-en/annee-2a-sn-fise-NDEN/choix-de-parcours-semestre-7-2a-sn-fise-N7ENA/semestre-7-sn-fise-parcours-image-et-multimedia-N7ENAM/base-de-la-programmation-fonct-et-traduction-des-langages-N7EN08/programmation-fonctionnelle-N7EN08A.html'
          university: 'ENSEEIHT'
          level: '1<sup>st</sup> year graduate'
        - title: 'Transition systems - TLA+'
          year: '2020/21--21/22'
          url: 'http://formations.enseeiht.fr/fr/offre-de-formations/diplome-d-ingenieur-FC_DI/diplome-D/ingenieur-enseeiht-informatique-et-telecommunications-program-n7i5-171/ingenieur-enseeiht-informatique-et-telecommunications-2eme-annee-subprogram-n7i52-181/annee-2a-sn-fise-NDEN/choix-de-parcours-semestre-8-2a-sn-fise-N8ENA/semestre-8-sn-fise-parcours-systemes-logiciels-N8ENAL/methodes-formelles-1-N8EN08/systemes-de-transition-N8EN08A.html'
          university: 'ENSEEIHT'
          level: '1<sup>st</sup> year graduate'
        - title: 'Automata and lexical analysis'
          year: '2020/21'
          url: 'http://www.informatique.univ-paris-diderot.fr/formations/licences/licence_enseignements_descriptifs#automates_et_analyse_lexicale_aal3'
          university: 'Université Paris Cité'
          level: '2<sup>nd</sup> year undergraduate'
        - title: 'Databases'
          year: '2020/21'
          url: 'http://www.informatique.univ-paris-diderot.fr/formations/licences/informatique-biologie/accueil'
          university: 'Université Paris Cité'
          level: '3<sup>rd</sup> year undergraduate and 1<sup>st</sup> graduate'
        - title: 'Computer science concepts 2'
          year: '2020/21'
          url: 'http://www.informatique.univ-paris-diderot.fr/formations/licences/licence_enseignements_descriptifs#concepts_informatiques_ci2'
          university: 'Université Paris Cité'
          level: '1<sup>st</sup> year undergraduate'
        - title: 'Programming basics 2'
          year: '2020/21'
          url: 'http://www.informatique.univ-paris-diderot.fr/formations/licences/licence_enseignements_descriptifs#initiation_a_la_programmation_2_ip2'
          university: 'Université Paris Cité'
          level: '1<sup>st</sup> year undergraduate'
        - title: 'Computability, decidability'
          year: '2017/18'
          url: 'http://www-licence.ufr-info-p6.jussieu.fr/lmd/licence/2017/ue/2I016-2018fev/'
          university: 'Sorbonne Université'
          level: '2<sup>nd</sup> year undergraduate'
        - title: 'Programming languages and compilation'
          year: '2016/17--18/19'
          url: 'https://www.lri.fr/~filliatr/ens/compil/'
          university: 'École normale supérieure'
          level: '3<sup>rd</sup> year undergraduate'
    design:
      # Choose how many columns the section has. Valid values: '1' or '2'.
      columns: '2'

  - block: academic
    id: academic
    content:
      title: Academic service
      items:
        - year: '2024'
          event: 'ATVA'
          url: 'https://atva-conference.org/2024/'
          role: 'artifact reviewer'
        - year: '2024'
          event: 'CAV'
          url: 'https://i-cav.org/2024/'
          role: 'artifact reviewer'
        - year: '2023'
          event: 'OOPSLA'
          url: 'https://2023.splashcon.org/track/splash-2023-oopsla/'
          role: 'reviewer and artifact reviewer'
        - year: '2023'
          event: 'JFLA'
          url: 'https://jfla.inria.fr/jfla2023.html'
          role: 'Program Committee member'
        - year: '2020'
          event: 'EMSOFT'
          url: 'https://esweek.org/emsoft/'
          role: 'reviewer'
        - year: '2019'
          event: 'EMSOFT'
          url: 'https://esweek.org/emsoft/'
          role: 'WiP Committee'
        - year: '2019'
          event: 'ECRTS'
          url: 'https://www.ecrts.org/'
          role: 'artifact reviewer'
        - year: '2018'
          event: 'EMSOFT'
          url: 'https://esweek.org/emsoft/'
          role: 'reviewer'
        - year: '2017'
          event: 'POPL'
          url: 'https://conf.researchr.org/home/POPL-2017'
          role: 'student volunteer'
        - year: '2017'
          event: 'PLDI'
          url: 'https://conf.researchr.org/home/pldi-2017'
          role: 'student volunteer'
    design:
      # Choose how many columns the section has. Valid values: '1' or '2'.
      columns: '2'

  - block: contact
    id: contact
    content:
      title: Contact
      subtitle: ''
      text: ''
      # Contact details - edit or remove options as needed
      email: lb@leliobrun.net
      phone: ''
      appointment_url: ''
      institution:
        name: National Institute of Informatics
        department:
        team:
      address:
        street: 2 Chome-1-2 Hitotsubashi
        city: Chiyoda City
        region: Tokyo
        postcode: 101-8430
        country: Japan
        country_code: JP
      # directions: Enter Building 1 and take the stairs to Office 200 on Floor 2
      # office_hours:
      #   - 'Monday 10:00 to 13:00'
      #   - 'Wednesday 09:00 to 10:00'
      # contact_links:
      #   - icon: twitter
      #     icon_pack: fab
      #     name: DM Me
      #     link: 'https://twitter.com/Twitter'
      #   - icon: skype
      #     icon_pack: fab
      #     name: Skype Me
      #     link: 'skype:echo123?call'
      #   - icon: video
      #     icon_pack: fas
      #     name: Zoom Me
      #     link: 'https://zoom.com'
      # Automatically link email and phone or display them just as text?
      autolink: true
      # Choose an email form provider (netlify/formspree)
      # form:
      #   provider: netlify
      #   formspree:
      #     # If using Formspree, enter your Formspree form ID
      #     id: ''
      #   netlify:
      #     # Enable CAPTCHA challenge to reduce spam?
      #     captcha: false
      # Coordinates to display a map - set your map provider in `params.yaml`
      coordinates:
        latitude: '35.6926313932514'
        longitude: '139.75791181780195'
    design:
      # Choose how many columns the section has. Valid values: '1' or '2'.
      columns: '2'
---
