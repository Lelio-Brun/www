---
title: "Automated Security Analysis for Real-World IoT Devices"
date: 2023-10
publishDate: 2023-10-29
authors: ["admin", "ihasuo", "tsekiyama", "yono"]
publication_types: ["1"]
abstract: "Automatic security protocol analysis is a fruitful research topic that demonstrates the application of formal methods to security analysis. Several endeavors in the last decades successfully verified security properties of large-scale network protocols like TLS, sometimes unveiling unknown vulnerabilities. In this work, we show how to apply these techniques to the domain of IoT, where security is a critical aspect. While most existing security analyses for IoT tackle individually either protocols, firmware or applications, our goal is to treat IoT systems as a whole. We focus our work on a case study, the Armadillo-IoT&nbsp;G4 device, highlighting the specific challenges we must tackle to analyze the security of a typical IoT device. We propose a model using the Tamarin prover, that allows us to state certain key security properties about the device and to prove them automatically."
# award: Best paper
featured: false
publication: "*International Workshop on Hardware and Architectural Support for Security and Privacy*" 
publication_short: "HASP'23"
url_publication: "https://www.haspworkshop.org/2023/"
tags: ["Solvers", "Internet of Things", "Formal Verification", "Cryptographic protocols"] 
doi: "10.1145/3623652.3623667"
# projects: ["zt-iot"]
url_slides: "slides.pdf"
# url_video: "https://youtu.be/DaPzcXwW9Kk"
---

