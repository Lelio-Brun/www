---
title: "Equation-Directed Axiomatization of Lustre Semantics to Enable Optimized Code Validation"
date: 2023-09
publishDate: 2023-09-12
authors: ["admin", "cgarion", "plgaroche", "xthirioux"]
publication_types: ["1"]
abstract: "Model-based design tools like SCADE Suite and Simulink are often used to design safety-critical embedded software. Consequently, generating correct code from such models is crucial. We tackle this challenge on Lustre, a dataflow synchronous language that embodies the concepts that base such tools. Instead of proving correct a whole code generator, we turn an existing compiler into a certifying compiler from Lustre to C, following a translation validation approach.We propose a solution that generates both C code and an attached specification expressing a correctness result for the generated and optionally optimized code. The specification yields proof obligations that are discharged by external solvers through the Frama-C platform."
award: Best paper
featured: false
publication: "*ACM Transactions on Embedded Computing Systems*" 
publication_short: "EMSOFT'23"
url_publication: "https://esweek.org/emsoft/"
tags: ["Solvers", "Stream Languages", "Verified Compilation"] 
doi: "10.1145/3609393"
projects: ["lustrec"]
url_slides: "slides/emsoft23"
# url_video: "https://youtu.be/DaPzcXwW9Kk"
---

