---
title: "Mechanized Semantics and Verified Compilation for a Dataflow Synchronous
Language with Reset"
date: 2020-01
publishDate: 2020-01-29T11:47:18.594169Z
authors: ["tbourke", "admin", "mpouzet"]
publication_types: ["1"]
abstract: "Specifications based on block diagrams and state machines are used to 
design control software, especially in the certified development of
safety-critical applications. 
Tools like SCADE Suite and Simulink/Stateflow are equipped with compilers to
translate such specifications into executable code. 
They provide programming languages for composing functions over streams as
typified by Dataflow Synchronous Languages like Lustre. 
<br/><br/>
Recent work builds on CompCert to specify and verify a compiler for the core of
Lustre in the Coq Interactive Theorem Prover. 
It formally links the stream-based semantics of the source language to the
sequential memory manipulations of generated assembly code.
We extend this work to treat a primitive for resetting subsystems. 
Our contributions include new semantic rules that are suitable for mechanized
reasoning, a novel intermediate language for generating optimized code, and
proofs of correctness for the associated compilation passes."
featured: false
publication: "*Proceedings of the 47th ACM SIGPLAN Symposium on Principles of
Programming Languages*" 
publication_short: "POPL'20"
url_publication: "https://popl20.sigplan.org/"
tags: ["Interactive Theorem Proving", "Stream Languages", "Verified
Compilation"] 
doi: "10.1145/3371112"
projects: ["velus"]
url_slides: "slides.pdf"
url_video: "https://youtu.be/DaPzcXwW9Kk"
---

