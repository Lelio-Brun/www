---
title: "Towards a Verified Lustre Compiler with Modular Reset"
date: 2018-05-27
publishDate: 2020-01-29T11:47:18.594431Z
authors: ["tbourke", "admin", "mpouzet"]
publication_types: ["1"]
abstract: "This paper presents ongoing work to add a modular reset construct to
a verified Lustre compiler.
We present a novel formal specification for the construct and sketch our plans
to integrate it into the compiler and its correctness proof."
featured: false
publication: "*Proceedings of the 21st International Workshop on Software and Compilers for Embedded Systems*"
publication_short: "SCOPES'18"
url_publication: "https://scopesconf.org/scopes-18/"
tags: ["Synchronous Languages", "Verified Compilation"]
doi: "10.1145/3207719.3207732"
projects: ["velus"]
url_slides: "slides.pdf"
---

