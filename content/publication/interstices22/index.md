---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Faites-vous confiance à votre thermostat ?"
authors: ["admin"]
date: 2022-10-25T10:03:56+09:00
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2022-10-25T10:03:56+09:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Interstices"
publication_short: ""
url_publication: "https://interstices.info"

abstract: "Certains systèmes logiciels qui interagissent avec le monde réel ne doivent échouer sous aucun prétexte, au risque de provoquer des catastrophes. Comment s’assurer que ces systèmes sont sûrs ? En particulier, quel degré de confiance accorder au processus de traduction de ces logiciels vers du code exécutable ?"

# Summary. An optional shortened abstract.
summary: ""

tags: ["Interactive Theorem Proving", "Stream Languages", "Verified Compilation"] 
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf:
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: https://interstices.info/faites-vous-confiance-a-votre-thermostat/ 
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: ["velus"]

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---
