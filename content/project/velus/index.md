---
date: '2019-11-04'
external_link: https://velus.inria.fr/
header:
  caption: ''
  image: ''
highlight: false
image_preview: ''
summary: A prototype compiler for Lustre, verified in [Coq](https://coq.inria.fr/).
tags: []
title: Vélus
---

