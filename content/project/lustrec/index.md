---
date: '2021-07-02'
external_link: https://cavale.enseeiht.fr/redmine/projects/lustrec/
header:
  caption: ''
  image: ''
highlight: false
image_preview: ''
summary: A prototype compiler for Lustre, with verification abilities and certifying
  compilation in mind.
tags: []
title: LustreC
---

