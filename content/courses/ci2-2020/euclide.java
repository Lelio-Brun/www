class Euclide {
    public static void main(String[] args) {
        int a = 420;
        int b = 126;
        while (a != b)
            if (a > b)
                a = a - b;
            else
                b = b - a;
        System.out.println(a);
    }
}
