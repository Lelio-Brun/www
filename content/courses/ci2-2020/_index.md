---
# Course title, summary, and position.
#linktitle: Présentation
#summary: Learn how to use Academic's docs layout for publishing online courses, software documentation, and tutorials.
weight: 1

# Page metadata.
title: CI2 2020
lastmod: "2020-04-01"
type: book
toc: false

---

Cette section contient un ensemble de notes permettant de poursuivre les TD du
cours Concepts Informatiques 2 de L1 à l'Université de Paris, dans le cadre de
la continuité pédagogique relative à la crise sanitaire du COVID-19. 
Ces notes s'adressent _a priori_ au groupe MI1, mais peuvent bien sûr être
réutilisées librement.
En particulier elles sont inspirées de [celles de Victor
Lanvin](https://vlanvin.fr/ci2/) pour le groupe INFO5.  

Le but est de permettre à chaque étudiant·e de poursuivre le cours à son rythme,
sans aucune obligation. 
Je vais tenter de garder cette section à jour pendant la durée du confinement,
au fur et à mesure de l'avancée du cours et des TD.
Je reste bien sûr disponible pour répondre à d'éventuelles questions par email.
