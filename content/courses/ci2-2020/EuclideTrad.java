class EuclideTrad {
    public static void main(String[] args) {
        int ic = 0;
        int[] mem = new int[100000];

        while (true) {
            switch (ic++) {

            // initialisations
            case 0: mem[0] = 420; break;
            case 1: mem[1] = 126; break;

            // test de boucle : si le test échoue on saute le corps de la boucle
            case 2: if (mem[0] == mem[1]) ic = 8; break; // <index de l'instruction après le while>

            // branchement
            case 3: if (mem[0] <= mem[1]) ic = 6; break; // <index de la 1ère instruction du else>
            case 4: mem[0] = mem[0] - mem[1]; break; // code du if
            case 5: ic = 7; break; // saut à l'<index de l'instruction après le branchement>
            case 6: mem[1] = mem[1] - mem[0]; break; // code du else

            case 7: ic = 2; break; // retour au test de la boucle

            case 8: System.out.println(mem[0]); break; // affichage
            case 9: System.exit(0);  // sortie
            }
        }
    }
}
