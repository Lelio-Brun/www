import java.util.Stack;

class PariteTrad {
    public static void main(String[] args) {
        int ic = 0;
        int[] mem = new int[100000];
        Stack<Integer> p = new Stack<>();

        while (true) {
            switch (ic++) {

            // initialisation
            case 000: mem[0] = 42; break;

            // appel à estPair
            case 001: p.push(ic); ic = 100; break; // <index de la 1ère instruction de estPair>
            case 002: System.exit(0); break; // fin du programme

            // estPair()
            case 100: if (mem[0] != 0) ic = 103; break;
            case 101: System.out.println("oui"); break;
            case 102: ic = 105; break;
            case 103: mem[0]--; break;
            case 104: p.push(ic); ic = 200; break; // appel à estImpair
            case 105: ic = p.pop(); break; // retour

            // estImpair()
            case 200: if (mem[0] != 0) ic = 203; break;
            case 201: System.out.println("non"); break;
            case 202: ic = 205; break;
            case 203: mem[0]--; break;
            case 204: p.push(ic); ic = 100; break; // appel à estPair
            case 205: ic = p.pop(); break; // retour
            }
        }
    }
}
