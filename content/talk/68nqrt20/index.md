---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Mechanized Semantics and Verified Compilation for a Dataflow Synchronous Language with Reset"
event: "68NQRT Seminar"
event_url: "http://68nqrt.inria.fr/"
location: "IRISA - Inria Rennes" 
abstract: "Les spécifications basées sur les schémas-blocs et machines à états
sont utilisées pour la conception de systèmes de contrôle-commande,
particulièrement dans le développement d'applications critiques. 
Des outils tels que SCADE et Simulink/Stateflow sont équipés de compilateurs qui
traduisent de telles spécifications en code exécutable.
Ils proposent des langages de programmation permettant de composer des fonctions
sur des flots, tel que l'illustre le langage synchrone à flots de données
\Lustre. 
</br></br>
Ce travail de thèse présente Vélus, un compilateur Lustre vérifié dans 
l'assistant de preuves interactif Coq.
Nous développons des modèles sémantiques pour les langages de la chaîne de 
compilation, et utilisons le compilateur C vérifié CompCert pour générer du code
exécutable et donner une preuve de correction de bout en bout.
Le défi principal est de montrer la préservation de la sémantique entre le
paradigme flots de données et le paradigme impératif, et de raisonner sur la
représentation bas niveau de l'état d'un programme.
</br></br>
En particulier, nous traitons le reset modulaire, une primitive pour 
réinitialiser des sous-systèmes.
Ceci implique la mise en place de modèles sémantiques adéquats, d'algorithmes
de compilation et des preuves de correction correspondantes.
Nous présentons un nouveau langage intermédiaire dans le schéma habituel de 
compilation modulaire dirigé par les horloges de Lustre.
Ceci débouche sur l'implémentation de passes de compilation permettant de 
générer un meilleur code séquentiel, et facilite le raisonnement sur la 
correction des transformations successives du reset modulaire."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2020-12-17T11:00:00-06:00
date_end: 
all_day: false

authors: []
tags: []

# Is this a featured talk? (true/false)
featured: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
url_slides:

url_code:
url_pdf:
url_video: "https://webconf.gricad.cloud.math.cnrs.fr/playback/presentation/2.0/playback.html?meetingId=d3fb43fc863f5a348d71036088aafe31f5e6de13-1608198158396"

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [velus]
---

