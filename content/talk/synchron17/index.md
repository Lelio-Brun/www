---
abstract: "We present two ideas in this talk:\n1) A co-inductive based semantics formalization for normalized Lustre\n2) A formalization of the semantics of the modular reset"
date: "2017-11-29T14:45:00"
event: "SYNCHRON'17"
event_url: "http://synchron17.inria.fr/"
highlight: false
location: "Centre Inria Rennes - Bretagne Atlantique"
address:
  street: "Avenue du Général Leclerc"
  city: "Rennes"
  region:
  postcode: "35042"
  country: "France"
math: false
selected: true
title: "Vélus: Towards a Modular Reset"
projects: ["velus"]
---
