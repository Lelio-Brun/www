---
title: "Verifying the Lustre Modular Reset"
date: 2018-11-29T09:00:00  # Schedule page publish date.
draft: false

# Abstract and optional shortened version.
abstract: "We present ongoing work on the compilation of the modular reset and its proof of correctness."

# Name of event and optional event URL.
event: "SYNCHRON'18"
event_url: "https://project.inria.fr/synchron2018/"
location: "CAES CNRS - Villa Clythia"
address:
  street: "2754, avenue Henri-Giraud"
  city: "Fréjus"
  region:
  postcode: "83600"
  country: "France"

# Is this a selected talk? (true/false)
selected: true
projects: ["velus"]
---
