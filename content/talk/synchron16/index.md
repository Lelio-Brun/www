---
abstract: "The compilation of block diagram languages like Lustre or Scade often use an intermediary language between the source data-flow code and the target imperative code. We recently implemented and verified a translation function in Coq from a simple intermediary language towards the CompCert-accepted Clight language.\n\nThe intermediary language uses an idealized hierarchical memory representation, whereas Clight uses an intricate memory model rather close of the machine. Our proof of correctness must consequently tackle alignment, aliasing and padding. We use separation logic predicates developed in CompCert to solve those issues."
abstract_short: ""
date: "2016-12-07T17:30:00"
event: "SYNCHRON'16"
event_url: "https://www.uni-bamberg.de/gdi/synchron-2016/"
highlight: false
location: "Dominikanerkloster Bamberg"
address:
  street: "Heiliggrabstraße 24"
  city: "Bamberg"
  region:
  postcode: "D-696052"
  country: "Germany"
selected: true
title: "Proving a Lustre Compiler Part 2"
projects: ["velus"]
---

The "Part 1" is a talk presented by {{< url tbourke >}}.
