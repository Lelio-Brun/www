---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

Title: "Sémantique Mécanisée, Compilation Vérifiée et Compilation
Certifiante pour Lustre"
event: "Séminaire SCALP"
location: "ISAE-SUPAERO" 
abstract: "Les spécifications basées sur les schémas-blocs et machines à états sont
utilisées pour la conception de systèmes de contrôle-commande,
particulièrement dans le développement d’applications critiques. Des
outils tels que Scade et Simulink/Stateflow sont équipés de compilateurs
qui traduisent de telles spécifications en code exécutable. Ils
proposent des langages de programmation permettant de composer des
fonctions sur des flots, tel que l’illustre le langage synchrone à flots
de données Lustre.
</br></br>
Cet exposé présente Vélus, un compilateur Lustre vérifié dans
l’assistant de preuves interactif Coq.  Nous développons des modèles
sémantiques pour les langages de la chaîne de compilation, et utilisons
le compilateur C vérifié CompCert pour générer du code exécutable et
donner une preuve de correction de bout en bout.  Le défi principal est
de montrer la préservation de la sémantique entre le paradigme flots de
données et le paradigme impératif, et de raisonner sur la représentation
bas niveau de l’état d’un programme.  En particulier, nous traitons le
reset modulaire, une primitive pour réinitialiser des sous-systèmes.
Ceci implique la mise en place de modèles sémantiques adéquats,
d’algorithmes de compilation et des preuves de correction
correspondantes.
</br></br>
Nous présenterons ensuite une autre approche de la compilation vérifiée,
dans le cadre d'un travail de recherche actuel sur le compilateur
LustreC.  Ce travail consiste à transformer LustreC en compilateur
certifiant, qui génère du code et de la spécification associée prouvée a
posteriori par des outils comme Frama-C ou SPARK."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2021-05-20T09:30:00
date_end: 
all_day: false

authors: []
tags: []

# Is this a featured talk? (true/false)
featured: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
url_slides:

url_code:
url_pdf:
url_video: "https://prismes.univ-toulouse.fr/player.php?code=70Bn133d"

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: ["velus", "lustrec"]
---

