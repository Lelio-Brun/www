---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Verified Compilation of the Modular Reset, Finally"
event: "SYNCHRON'19"
event_url: "http://synchron19.org/"
abstract: "We present the formalization of the semantics of the modular reset construct of Lustre, and show how to integrate it into Vélus, a Lustre compiler verified in Coq and built on top of CompCert.
In particular, we introduce a new intermediate language in the compilation chain, specifically designed to handle compilation of the modular reset."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2019-11-28T09:45:00
#date_end: 2019-12-03T10:26:40+01:00
all_day: false
location: "CAES CNRS - Centre Paul-Langevin"
address:
  street: "24, rue du Coin"
  city: "Aussois"
  region:
  postcode: "73500"
  country: "France"

# Is this a featured talk? (true/false)
featured: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
url_slides:

url_code:
url_pdf:
url_video:

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: ["velus"]
---
