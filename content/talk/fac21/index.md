---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Mechanized Semantics and Verified Compilation for a Dataflow Synchronous Language with Reset"
event: "FAC'21"
event_url: "http://projects.laas.fr/IFSE/FAC/"
location: "ENAC" 
summary:
abstract: "Specifications based on block diagrams and state machines are used to 
design control software, especially in the certified development of
safety-critical applications. 
Tools like SCADE Suite and Simulink/Stateflow are equipped with compilers to
translate such specifications into executable code. 
They provide programming languages for composing functions over streams as
typified by Dataflow Synchronous Languages like Lustre. 
Recent work builds on CompCert to specify and verify a compiler for the core of
Lustre in the Coq Interactive Theorem Prover. 
It formally links the stream-based semantics of the source language to the
sequential memory manipulations of generated assembly code.
We extend this work to treat a primitive for resetting subsystems. 
Our contributions include new semantic rules that are suitable for mechanized
reasoning, a novel intermediate language for generating optimized code, and
proofs of correctness for the associated compilation passes."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2021-10-14T15:30:00
date_end: 
all_day: false

# Schedule page publish date (NOT talk date).

authors: []
tags: []

# Is this a featured talk? (true/false)
featured: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
url_slides:

url_code:
url_pdf:
url_video: 

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [velus]
---
