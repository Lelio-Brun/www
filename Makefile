ROOT=public
NETLIFY=netlify.toml
GO:=$(shell go version | grep -Po "go\K[0-9.]+")
HUGO:=$(shell hugo version | grep -Po "v\K[0-9.]+")

.PHONY: all local deploy clean cv

all: $(NETLIFY)
	@hugo

clean:
	@rm -rf $(ROOT)

local:
	@hugo server --navigateToChanged --disableFastRender

cv:
	@cd academic-to-awesome-cv; dune exec ./main.exe
	@cp academic-to-awesome-cv/CV.pdf static/files

$(NETLIFY): FORCE
	@rm -f $@
	@sed -e "s|%%GO%%|$(GO)|" -e "s|%%HUGO%%|$(HUGO)|" < $@.in > $@

FORCE:
