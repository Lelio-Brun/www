module my_website

go 1.19

require (
	github.com/HugoBlox/hugo-blox-builder/modules/blox-bootstrap/v5 v5.9.7
	github.com/HugoBlox/hugo-blox-builder/modules/blox-plugin-netlify v1.1.2-0.20240513194541-c2e9a799f797
	github.com/HugoBlox/hugo-blox-builder/modules/blox-plugin-reveal v1.1.2
)

require (
	github.com/HugoBlox/hugo-blox-builder/modules/blox-core v0.3.1 // indirect
	github.com/HugoBlox/hugo-blox-builder/modules/blox-plugin-decap-cms v0.1.1 // indirect
	github.com/HugoBlox/hugo-blox-builder/modules/blox-seo v0.2.2 // indirect
)
